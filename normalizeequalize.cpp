#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

using namespace std;
using namespace cv;

Mat src; Mat srcGray; Mat dstNorm; Mat dstEq;


int main( int argc, char** argv )
{
    /// Load the source image
    src = imread( argv[1], 1 );
	cvtColor(src, srcGray, CV_BGR2GRAY);
    namedWindow("Input Image", WINDOW_AUTOSIZE );
    imshow("Input Image", srcGray);

	// Image Normalization
    dstNorm = srcGray.clone();
	normalize(srcGray, dstNorm, 0, 255, NORM_MINMAX, CV_8UC1);

    namedWindow("Normalized Image", WINDOW_AUTOSIZE );
    imshow("Normalized Image", dstNorm);

	// Histogram Equalization
	equalizeHist(srcGray, dstEq);

	namedWindow("Histogram Equalized Image", WINDOW_AUTOSIZE);
	imshow("Histogram Equalized Image", dstEq);


	// draw histograms of all three images
	// http://docs.opencv.org/2.4/doc/tutorials/imgproc/histograms/histogram_calculation/histogram_calculation.html
	int histSize = 256; //from 0 to 255
	/// Set the ranges ( for B,G,R) )
	float range[] = { 0, 256 }; //the upper boundary is exclusive
	const float* histRange = { range };
	bool uniform = true; bool accumulate = false;
	Mat src_hist, norm_hist, eq_hist;

	calcHist(&srcGray, 1, 0, Mat(), src_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&dstNorm, 1, 0, Mat(), norm_hist, 1, &histSize, &histRange, uniform, accumulate);
	calcHist(&dstEq, 1, 0, Mat(), eq_hist, 1, &histSize, &histRange, uniform, accumulate);

	// Draw the histograms for B, G and R
	int hist_w = 512; int hist_h = 400;
	int bin_w = cvRound((double)hist_w / histSize);

	Mat histImageSrc(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));
	Mat histImageNorm(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));
	Mat histImageEq(hist_h, hist_w, CV_8UC3, Scalar(0, 0, 0));

	/// Normalize the result to [ 0, histImage.rows ]
	normalize(src_hist, src_hist, 0, histImageSrc.rows, NORM_MINMAX, -1, Mat());
	normalize(norm_hist, norm_hist, 0, histImageNorm.rows, NORM_MINMAX, -1, Mat());
	normalize(eq_hist, eq_hist, 0, histImageEq.rows, NORM_MINMAX, -1, Mat());

	/// Draw for each channel
	for (int i = 1; i < histSize; i++)
	{
		line(histImageSrc, Point(bin_w*(i - 1), hist_h - cvRound(src_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(src_hist.at<float>(i))),
			Scalar(255, 0, 0), 2, 8, 0);
		line(histImageNorm, Point(bin_w*(i - 1), hist_h - cvRound(norm_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(norm_hist.at<float>(i))),
			Scalar(0, 255, 0), 2, 8, 0);
		line(histImageEq, Point(bin_w*(i - 1), hist_h - cvRound(eq_hist.at<float>(i - 1))),
			Point(bin_w*(i), hist_h - cvRound(eq_hist.at<float>(i))),
			Scalar(0, 0, 255), 2, 8, 0);
	}

	/// Display
	namedWindow("histogram src", CV_WINDOW_AUTOSIZE);	imshow("histogram src", histImageSrc);
	namedWindow("histogram normalize", CV_WINDOW_AUTOSIZE);	imshow("histogram normalize", histImageNorm);
	namedWindow("histogram equalize", CV_WINDOW_AUTOSIZE);	imshow("histogram equalize", histImageEq);

    waitKey();
    return 0;
}