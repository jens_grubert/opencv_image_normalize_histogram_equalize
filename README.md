OpenCV Project to show the differences between image normalization and histogram equalization.


Image Normalization:
http://docs.opencv.org/2.4.8/modules/core/doc/operations_on_arrays.html#normalize
https://en.wikipedia.org/wiki/Normalization_(image_processing)

Histogram Equalization:
http://docs.opencv.org/2.4/doc/tutorials/imgproc/histograms/histogram_equalization/histogram_equalization.html
https://en.wikipedia.org/wiki/Histogram_equalization